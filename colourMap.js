
var canvas;
var markObjects; //contain all plots in order of the event ids.
var roomTexts;

var fetchObjects = function(callback){
   canvas = new fabric.Canvas('c1');
  //raw data from ajax call and their location
  var data = [ {name: "Bed room", colour: "red", locations: [{left: 0,top: 0, width: 220, height: 220}]  },

                {name: "Kitchen", colour: "purple", locations: [{left: 220,top: canvas.height-180, width: 180, height: 190}]  },

                {name: "Livingroom", colour: "green", locations: [{left: 220,top: 0, width: canvas.width-220, height: 220}]  },
                {name: "Bathroom", colour: "brown", locations: [{left: 400,top: canvas.height-180, width: 230, height: 190}]  },
                {name: "Kidsroom", colour: "blue", locations: [{left: 0,top: canvas.height-180, width: 220, height: 190}]  },

                {name: "Floor", colour: "orange", locations: [{left: 0,top: 221, width: canvas.width, height:100}]  }];


  var markPoints = [   
                      {id: 1, name: "sleeping", image: "sleeping.jpg", locations: [{left: 10, top: 400}, {left: 12, top: 4}, {left: 120, top: 200}, {left: 122, top: 24}]},

                      {id: 2, name: "Fallen", image: "falling.png", locations: [{left: 200, top: 12}, {left: 33, top: 24}, {left: 400, top: 142}, {left: 343, top: 424}, {left: 240, top: 122}, {left: 334, top: 242}]},

                      {id: 3, name: "standing", image: "standing.png", locations: [{left: 100, top: 112}, {left: 121, top: 142}, {left: 500, top: 112}, {left: 621, top: 142}, {left: 551, top: 112}, {left: 351, top: 142}]},

                      {id: 4, name: "Drinking", image: "drinking.png", locations: [{left: 310, top: 332}, {left: 212, top: 314}, {left: 410, top: 32}, {left: 552, top: 34}, {left: 510, top: 32}, {left: 712, top: 14}]}];


  var rooms = new Array();
  roomTexts = new Array();
  /*var rooms = new fabric.Group(new Array(), {
        left: 0,
        top: 0    
      }); 
  */
  for(var i =0; i<data.length; i++){
      var dim = data[i].locations[0];
      var colour = data[i].colour;

      var text = new fabric.Text(data[i].name, {fontSize: 30,fill: 'white',opacity: 0.4, originX: 'center', originY: 'center'});
      roomTexts.push(text);

      var shape  = new fabric.Rect({dat: data[i] ,  originX: 'center', originY: 'center', fill: colour,opacity: 0.8,  width: dim.width, height: dim.height, selectable: false,strokeWidth: 1, stroke: 'rgba(0,0,0,0.5)' });        

      /*shape.on('mousedown', function() {
         console.log('You clicked on me and my name is '+ this.dat.name);
         $("#dialogCustomText").text("I am "+ this.dat.name);
         $("#dialogCustomText").append("<p>For periods from x to y -> </p><p>Total drinking events are: z</p>");

         $( "#dialog" ).dialog( "open" );
      });

      shape.on('mouseover', function() {
        this.set({stroke: 'rgba(255,255,255,1)', strokeWidth: 1});
        
         //console.log('hover on  '+ this.dat.name);
         canvas.renderAll();
      });

      shape.on('mouseout', function() {
        this.set({stroke: 'rgba(0,0,0,0.5)', strokeWidth: 1});        

        // console.log('hover out on  '+ this.dat.name);
         canvas.renderAll();
      });
      */

      var shape_grp = new fabric.Group([ shape, text ], {
        id: "room",
        left: dim.left,
        top: dim.top,
        selectable: false         
      });


      shape_grp.on('mousedown', function() {
         console.log('You clicked on me and my name is '+ this.item(0).dat.name);
         $("#dialogCustomText").text("I am "+ this.item(0).dat.name);
         $("#dialogCustomText").append("<p>For periods from x to y -> </p><p>Total drinking events are: z</p>");

         $( "#dialog" ).dialog( "open" );
      });

      shape_grp.on('mouseover', function() {
        this.item(0).set({stroke: 'rgba(255,255,255,1)', strokeWidth: 1});
        
         //console.log('hover on  '+ this.dat.name);
         canvas.renderAll();
      });

      shape_grp.on('mouseout', function() {
        this.item(0).set({stroke: 'rgba(0,0,0,0.5)', strokeWidth: 1});        

        // console.log('hover out on  '+ this.dat.name);
         canvas.renderAll();
      });

      rooms.push(shape_grp);
      //rooms.add(shape);
  }
  

  markObjects = new Array();
  var flagWait = true;
  var j=0;
   for(j = 0; j< markPoints.length; j++){                  

      var group = new fabric.Group(new Array(), {
            id: j+1,
            left: 0,
            top: 0    
          });      
      for (var i=0; i < markPoints[j].locations.length; i++) {
          flagWait = true;    
          var img1 =   fabric.Image.fromURL('symbols/'+markPoints[j].image,{grp: group, i_index: i, j_index: j}, function(img, data){              
                       
                      img.set({left: markPoints[data.j_index].locations[data.i_index].left, top: markPoints[data.j_index].locations[data.i_index].top}).scale(1);                      
                      data.grp.add(img);                                           
                      if(data.i_index+1 == markPoints[data.j_index].locations.length) {
                         //console.log("last item of same group, so add them in object now");
                         markObjects[data.j_index] = data.grp;

                         //console.log("id is "+markObjects[data.j_index].id);
                         //console.log("values are "+ (data.j_index));
                      }

                      if(data.j_index+1 == markPoints.length && data.i_index+1 == markPoints[markPoints.length-1].locations.length){
                         console.log("last element, now calling callback");                         
                        
                         callback(rooms, markObjects);

                      }                                            
          });                     
      }    
   }
   
}


var createMap   = function () {
      fetchObjects(function(rooms, markObjects){
          
          // adding rooms to canvas and animting them
          for(var  i = 0; i< rooms.length; i++){

            canvas.add(rooms[i]);           
           
            rooms[i].set({left: rooms[i].left+1000});
            rooms[i].animate('left', rooms[i].left-1000, {
              onChange: canvas.renderAll.bind(canvas),
              duration: 1000,
              easing: fabric.util.ease.easeOutExpo
            });
          }
          /*
          rooms.set({left: rooms.left+1000});
          rooms.animate('left', rooms.left-1000, {
            onChange: canvas.renderAll.bind(canvas),
            duration: 1000,
            easing: fabric.util.ease.easeOutExpo
          });

          canvas.add(rooms);
          */

          for(var j =0; j< markObjects.length; j++)
          {
            canvas.add(markObjects[j]);
          }
          
          canvas.hoverCursor = 'pointer';
          console.log(" cursor is"+canvas.hoverCursor);
          canvas.renderAll();        



          //logic test
          canvas.item(0).set({left: 1});
          //console.log("left element of got is "+ canvas.item(2).left);
          /*
          var img1 =   fabric.Image.fromURL('symbols/standing.png',{}, function(img,x){
            canvas.add(img);
            
            img.set({left: 200, top: 100}).scale(1);
            img.set('left', 300);
            canvas.renderAll();
          });      
          
    
          canvas.on('mouse:down', function(options) {
            console.log('an object was clicked! ', options.target.type);
            if (options.target) {
              console.log('an object was clicked! ', options.target.type);
            }
          });
          */

      });      
           
}


/**
* Object contains array of array eg. [[drinking1, drinking2], [standing1, standing2 ] ]
* choice is udes to address the index of the event to be shown
*
*/
var togglePlots = function(enable, choice){
  //console.log("this is canvas objects"+ canvas);
  var allObjects = canvas.getObjects();
  //console.log("all objects are "+ allObjects[9].id);

  if(enable==1){
    
    for(var i = 0; i < allObjects.length; i++){
      if(allObjects[i].id == choice){
        console.log("already there! with id "+choice+" hence doing nothing");
        return;
      }      

    }
    //not found so add the element with the choice
    console.log("adding the element group with id"+ choice);
    canvas.add(markObjects[choice-1]);
    canvas.renderAll();


  }

  //if need to remove
  else if(enable == 0){

    for(var i = 0; i < allObjects.length; i++){
      console.log("current id is"+ allObjects[i].id);
      if(allObjects[i].id == choice){
        console.log("Foound! with id "+choice+" hence now removing this element");
        canvas.remove(allObjects[i]);
        canvas.renderAll();
        return;
      }      
    }

    console.log("Not displayed on screen.");

  }
}


var toggleNames = function(enable){

  var allObjects = canvas.getObjects();
  if(enable==0){
    for(var i = 0; i < allObjects.length; i++){
      if(allObjects[i].id == "room"){
        if(allObjects[i].item(1)){
          allObjects[i].remove(allObjects[i].item(1));          
        }
        else{
          console.log("No element on sreen to delete.");
          return;
        }
          
      }
    }        
  }
  //in case to display it on screen.
  else{
    var txtObjIndex = 0;
    for(var i = 0; i < allObjects.length; i++){

      if(allObjects[i].id == "room"){
        if(allObjects[i].item(1)){
          console.log("already there.");
          return;
        }
        else{
          allObjects[i].add(roomTexts[txtObjIndex]);
          txtObjIndex++;
        }
      }

    } 
  }
  setTimeout(function(){canvas.renderAll();}, 100);  
}
  
